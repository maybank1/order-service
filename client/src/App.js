import logo from './logo.svg';
import './App.css';
import { useEffect } from 'react';
import { useState } from 'react';

function App() {
  const [datas, setDatas] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch("http://localhost:8889/api/order");
      const res = await response.json();
      setDatas(res);
    }

    getData();
  }, [])

  return (
    <div className="App">
      <div>
        {datas.map(data => {
          const items = data.orderLineItems;
          for (let i = 0; i < items.length; i++) {
            return (
              <div key={items[i].id}>
                {items[i].price}
              </div>
            )
          }
        })}
      </div>
    </div>
  );
}

export default App;
