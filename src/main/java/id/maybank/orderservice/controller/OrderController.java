package id.maybank.orderservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.maybank.orderservice.model.Order;
import id.maybank.orderservice.model.OrderLineItems;
import id.maybank.orderservice.model.dto.OrderLineItemsDTO;
import id.maybank.orderservice.model.dto.OrderRequest;
import id.maybank.orderservice.service.OrderService;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public String placeOrder(@RequestBody OrderRequest orderRequest) {
        return orderService.placeOrder(orderRequest);
    }

    @PostMapping("/{id}")
    @ResponseStatus(value = HttpStatus.CREATED)
    public String placeItemToOrder(@PathVariable Long id, @RequestBody OrderLineItemsDTO orderLineItemsDTO) {
        return orderService.placeItemToOrder(id, orderLineItemsDTO);

    }

    @GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    public List<Order> getOrders() {
        return orderService.getOrders();
    }

    @GetMapping("/{idOrder}/{idItem}")
    @ResponseStatus(value = HttpStatus.OK)
    public OrderLineItemsDTO getItem(@PathVariable(name = "idOrder") Long idOrder,
            @PathVariable(name = "idItem") Long idItem) {
        return orderService.getItem(idOrder, idItem);
    }

    @GetMapping("/delete/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public String deleteOrder(@PathVariable Long id) {
        orderService.deleteOrderById(id);
        return "Order " + id + " has been deleted";
    }

    @GetMapping("/delete/{idOrder}/{idItem}")
    @ResponseStatus(value = HttpStatus.OK)
    public Map<String, String> deleteItem(@PathVariable(name = "idOrder") Long idOrder,
            @PathVariable(name = "idItem") Long idItem) {
        return orderService.deleteItemById(idOrder, idItem);
    }

    @PostMapping("/update")
    @ResponseStatus(value = HttpStatus.OK)
    public OrderLineItemsDTO updateItem(@RequestBody OrderLineItems orderLineItems) {
        return orderService.updateOrderItem(orderLineItems);
    }
}
