package id.maybank.orderservice.model;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;

@Entity(name = "t_orders")
@Data
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String orderNumber;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<OrderLineItems> orderLineItems;

    // public void getOrderNumber(String orderNumber) {
    // this.orderNumber = String.format("{}/{}/{}", LocalDate.now().getMonthValue(),
    // LocalDate.now().getDayOfMonth(), LocalDate.now(), LocalDate.now().getYear());
    // }
}
