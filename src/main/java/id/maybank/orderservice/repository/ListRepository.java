package id.maybank.orderservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
// import org.springframework.data.jpa.repository.Modifying;
// import org.springframework.data.jpa.repository.Query;
// import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.maybank.orderservice.model.OrderLineItems;

@Repository
public interface ListRepository extends JpaRepository<OrderLineItems, Long> {

    // @Modifying
    // @Transactional
    // @Query(nativeQuery = true, value = "DELETE FROM t_order_line_items WHERE id =
    // :id")
    // void deleteById(@Param("id") Long id);
    // @Transactional
    // public void deleteItemBySkuCode(String skuCode);
}
