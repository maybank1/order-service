package id.maybank.orderservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.maybank.orderservice.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{
    
}
