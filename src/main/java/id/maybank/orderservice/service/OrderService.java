package id.maybank.orderservice.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import id.maybank.orderservice.model.Order;
import id.maybank.orderservice.model.OrderLineItems;
import id.maybank.orderservice.model.dto.InventoryResponse;
import id.maybank.orderservice.model.dto.OrderLineItemsDTO;
import id.maybank.orderservice.model.dto.OrderRequest;
import id.maybank.orderservice.repository.ListRepository;
import id.maybank.orderservice.repository.OrderRepository;

@Service
@Transactional
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ListRepository listRepository;

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private WebClient.Builder webClientBuilder;

    public String placeOrder(OrderRequest orderRequest) {
        Order order = new Order();
        String orderNumber = UUID.randomUUID().toString();
        order.setOrderNumber(orderNumber);
        List<OrderLineItems> orderLineItems = orderRequest.getOrderLineItemsDTOs().stream()
                .map(orderItem -> mapToEntity(orderItem, orderNumber)).toList();
        order.setOrderLineItems(orderLineItems);

        // get all skuCode
        List<String> skuCodes = order.getOrderLineItems().stream().map(i -> i.getSkuCode()).toList();

        // connect to inventory service
        InventoryResponse[] inventoryResponses = webClientBuilder.build().get()
                .uri("lb://inventory-service/api/inventory",
                        uriBuilder -> uriBuilder.queryParam("skuCode", skuCodes).build())
                .retrieve()
                .bodyToMono(InventoryResponse[].class)
                .block();

        boolean res = Arrays.stream(inventoryResponses).allMatch(InventoryResponse::isInStock);

        if (res) {
            orderRepository.save(order);
        } else {
            throw new IllegalArgumentException("Product not in stock");
        }

        // call inventory controller instock

        // response order result

        return "Order " + order.getOrderNumber() + " has been placed " + inventoryResponses[0].getHostname() + " "
                + inventoryResponses[0].getPort();
    }

    public String placeItemToOrder(Long id, OrderLineItemsDTO orderLineItemsDTO) {
        Order order = orderRepository.findById(id).orElse(null);
        List<OrderLineItems> orderLineItems = order.getOrderLineItems();
        OrderLineItems newItem = new OrderLineItems();
        newItem.setOrderNumber(order.getOrderNumber());
        newItem.setPrice(orderLineItemsDTO.getPrice());
        newItem.setSkuCode(orderLineItemsDTO.getSkuCode());
        newItem.setQty(orderLineItemsDTO.getQty());
        orderLineItems.add(newItem);
        orderRepository.save(order);
        return "Item on Order id " + id + " has been created";
    }

    private OrderLineItems mapToEntity(OrderLineItemsDTO lineItemDTO, String orderNumber) {
        OrderLineItems orderLineItems = modelMapper.map(lineItemDTO, OrderLineItems.class);
        orderLineItems.setOrderNumber(orderNumber);
        return orderLineItems;
    }

    public List<Order> getOrders() {
        return orderRepository.findAll();
    }

    public OrderLineItemsDTO getItem(Long idOrder, Long idItem) {
        List<OrderLineItems> order = orderRepository.findById(idOrder).orElse(null).getOrderLineItems();
        OrderLineItems item = listRepository.findById(idItem).orElse(null);

        if (order.contains(item)) {
            OrderLineItemsDTO getItem = modelMapper.map(item, OrderLineItemsDTO.class);
            return getItem;
        }
        return new OrderLineItemsDTO();
    }

    public Order getOrderById(Long id) {
        return orderRepository.findById(id).orElse(null);
    }

    // delete order
    public void deleteOrderById(Long id) {
        orderRepository.deleteById(id);
    }

    public Map<String, String> deleteItemById(Long idOrder, Long idItem) {
        Order order = orderRepository.findById(idOrder).orElse(null);
        List<OrderLineItems> orderLineItems = order.getOrderLineItems();
        OrderLineItems item = listRepository.findById(idItem).orElse(null);
        // listRepository.deleteById(item.getId());
        Map<String, String> res = new HashMap<>();
        if (orderLineItems.contains(item)) {
            orderLineItems.remove(item);
            listRepository.saveAllAndFlush(orderLineItems);
            res.put("message",
                    "Item id " + idItem + " from order id " + idOrder + " has been deleted ");
        } else {
            res.put("message", "Wrong order id or item id");
        }
        return res;
    }

    // update order
    public OrderLineItemsDTO updateOrderItem(OrderLineItems orderLineItems) {
        // Map<String, String> res = new HashMap<>();
        OrderLineItems item = new OrderLineItems();
        item.setId(orderLineItems.getId());
        item.setOrderNumber(orderLineItems.getOrderNumber());
        item.setSkuCode(orderLineItems.getSkuCode());
        item.setPrice(orderLineItems.getPrice());
        item.setQty(orderLineItems.getQty());
        OrderLineItemsDTO orderLineItemsDTO = modelMapper.map(item, OrderLineItemsDTO.class);
        listRepository.save(item);
        // if (items.contains(orderLineItems)) {
        // res.put("message", "Item has been updated");
        return orderLineItemsDTO;
        // } else {
        // res.put("message", "Wrong itemId");
        // return new OrderLineItemsDTO();
        // }
        // return res;

    }
}
